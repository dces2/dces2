var koza23 = db.events.findOne({_id: "koza23"});
var contests = koza23.contests;

for (let contest of contests) {
    let id = contest.id;
    contest.tables = [];

    if (/travel.*/.test(id)) {
        let right = "participant";
        if (/.*ru.*/.test(id))
            right += "~language=ru";
        if (/.*en.*/.test(id))
            right += "~language=en";

        let league

        if (/.*1.*/.test(id))
            league = "1"
        if (/.*2.*/.test(id))
            league = "2"

        if (/.*test/.test(id))
            league = "1" + league

        right += "~league=" + league;

        contest.rights = [right];
    }

    contest['page sizes'] = [1];
    if (/.*test.*/.test(id))
        contest.start = new Date(2022, 11 - 1, 2, 10, 0, 0, 0);
    else
        contest.start = new Date(2023, 11 - 1, 2, 10, 0, 0, 0);

    contest.finish = new Date(2023, 11 - 1, 2, 12, 0, 0, 0);
    contest.results = new Date(2023, 11 - 1, 3, 23, 0, 0, 0)
    contest.duration = 0;
    contest["allow restart"] = false;
    contest["only admin"] = false;
    contest["results translators"] = [{"type": "kiojs"}];

    print("processed contest " + id + " as a usual contest");
}

db.events.update(
    {_id: "koza23"},
    {
        $set: {
            contests: contests
        }
    }
);
