var koza22 = db.events.findOne({_id: "koza22"});
var contests = koza22.contests;

for (let contest of contests) {
    let id = contest.id;
    contest.tables = [];

    let right = "participant";

    if (/.*-ru/.test(contest.id))
        right += "~language=ru";
    else
        right += "~language=en";

    if (/[a-z]+1-../.test(contest.id))
        right += "~league=1";
    else if (/[a-z]+2-../.test(contest.id))
        right += "~league=2";

    contest.rights = [right];

    contest['page sizes'] = [1];
    contest.start = new Date(2022, 11 - 1, 25, 10, 0, 0, 0);
    contest.finish = new Date(2022, 11 - 1, 25, 12, 0, 0, 0);
    contest.results = new Date(2022, 11 - 1, 26, 23, 0, 0, 0)
    contest.duration = 0;
    contest["allow restart"] = false;
    contest["only admin"] = false;
    contest["results translators"] = [{"type": "kiojs"}];

    print("processed contest " + contest.id + " as a usual contest");
}

db.events.update(
    {_id: "koza22"},
    {
        $set: {
            contests: contests
        }
    }
);
