var koza20 = db.events.findOne({_id: "koza20"});
var koza21 = db.events.findOne({_id: "koza21"});
var koza23 = db.events.findOne({_id: "koza23"});
var contests20 = koza20.contests;
var contests21 = koza21.contests;
var contests23 = koza23.contests;

let contests = []

for (let contest of contests20) {
    let id = contest.id;
    if (/task1.*/.test(id))
        contests.push(contest)
}

for (let contest of contests21) {
    let id = contest.id;
    if (/elasticity.*/.test(id))
        contests.push(contest)
}

// print(contests.map(function (c) {return c.id}));


db.events.update(
    {_id: "koza23"},
    {
        $set: {
            contests: contests
        }
    }
);
