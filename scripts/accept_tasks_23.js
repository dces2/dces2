var event_id = 'bebras23';
var year = 2023;

function user(login) {
    return db.users.findOne({event_id: event_id, login: login});
}

function uid(login) {
    return user(login)._id;
}

function pid(name) {
    return db.categories.findOne({link: {$regex: year + '/' + name + '$'}}).pid;
}

function grade_2_contest(g) {
    g = +g;
    if (g === 11)
        var gg = '11';
    else if (g % 2 === 0)
        gg = (g - 1) + '-' + g;
    else
        gg = g + '-' + (g + 1);

    return 'contest-' + event_id + '_' + gg;
}

function sub(login, task) {
    var u = user(login);
    var t = pid(task);
    var g = u.grade;
    var c = grade_2_contest(g);

    db[c].find({u: u._id, pid: t}, {lt: 1, st: 1, a: 1}).forEach(function (d) {
        printjson(d);
    })
}

function suball(login) {
    var u = user(login);
    var g = u.grade;
    var c = grade_2_contest(g);

    return db[c].find({u: u._id}).toArray();
}

tasks = {
    'D3': {
        r: 2,
        ans: '{"7":-1,"8":-1,"9":0,"10":2,"11":1,"12":5,"13":3,"14":4,"15":6}'
    },
    'D1': {
        r: 2,
        ans: '{"5":4,"6":3,"7":2,"8":1}'
    }
    /*'NZ-02': {
        r: 2,
        ans: '43'
    },
    'IE-09': {
        a: 0
    }*/
};

var accept = [
    ['68460952.14', 'D1'],
    ['68452001.44', 'D1'], //гришаев
    ['68451652.1', 'D1'], //даниэль шаповал

    ['86637491.3', 'D3'], //грошев
    ['86637494.1', 'D3'], //черкашин

    ['83341484.64', 'D3'],
    ['83341484.62', 'D3'],
    ['83341484.61', 'D3'],
    ['81058323.9', 'D3']
];

for (var i = 0; i < accept.length; i++) {
    var login = accept[i][0];
    var task = accept[i][1];

    var p = pid(task);
    var u = user(login);

    var task_answer_info = tasks[task];

    var task_answer;
    if ('a' in task_answer_info)
        task_answer = {"a": task_answer_info.a};
    else
        task_answer = {"r": tasks[task].r, "s": tasks[task].ans};

    var submission = {
        "u": u._id,
        "lt": NumberLong(39 * 60 * 1000), //39 Min
        "st": ISODate("2023-12-23T09:28:10.676Z"),
        "pid": p,
        "a": task_answer
    };

    var contest = grade_2_contest(u.grade);

    print("inserting for user " + login + "=" + u.surname + " to " + contest + ": ");
    printjson(submission);
    // db[contest].insert(submission);
}
