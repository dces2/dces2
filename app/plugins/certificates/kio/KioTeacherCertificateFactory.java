package plugins.certificates.kio;

import models.User;
import models.newserialization.Deserializer;
import models.newserialization.Serializer;
import plugins.certificates.Diploma;
import plugins.certificates.DiplomaFactory;

public class KioTeacherCertificateFactory extends DiplomaFactory {

    private int year;
    private int minimalNumberOfParticipants;

    @Override
    public Diploma getDiploma(User user) {
        return new KioTeacherCertificate(user, this);
    }

    public int getYear() {
        return year;
    }

    public int getMinimumNumberOfParticipants() {
        return minimalNumberOfParticipants;
    }

    @Override
    public void serialize(Serializer serializer) {
        super.serialize(serializer);
        serializer.write("year", year);
        serializer.write("participants", minimalNumberOfParticipants);
    }

    @Override
    public void update(Deserializer deserializer) {
        super.update(deserializer);
        year = deserializer.readInt("year");
        minimalNumberOfParticipants = deserializer.readInt("participants", 5);
    }
}
