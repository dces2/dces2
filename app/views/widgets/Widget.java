package views.widgets;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ilya
 * Date: 16.08.13
 * Time: 15:16
 */
public interface Widget {

    List<ResourceLink> links();

}
