package models.newserialization;

/**
 * Created by ilya
 */
public interface Updatable {

    void update(Deserializer deserializer);

}
