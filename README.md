# Installation guide
1. Use `mongorestore` to create a database from `dces2-minimal.zip`
2. In the `conf` folder, put the `application.conf`:

```
play.crypto.secret="Hfhk^&8*878uHfIUFDY*&&)UHn;klsfdjakjh_)(*)SD(Flakjsa;ajfdsajhbBBosd"
play.i18n.langs=["en", "ru"]

mongodb.db=dces2-minimal
mongodb.host=localhost
mongodb.port=27017

resources.local=false

salt=32c1b7341233fabc371cd5a856bcf49a #exactly 16 bytes in HEX needed

play.http.parser.maxMemoryBuffer=512K

resources_version="13"
```

2. In the `conf` folder, copy `application.ini.example` to `application.ini` and modify it.
3. Start application by the command from the file `sbt-run-example`.
4. Go to [http://localhost:9000/debug/enter](http://localhost:9000/debug/enter) and use login `admin` and password `eroack92`.